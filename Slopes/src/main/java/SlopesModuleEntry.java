import com.inwady.editor.Module;
import com.inwady.editor.Point;
import com.inwady.editor.Route;

import java.util.ArrayList;
import java.util.Vector;

public class SlopesModuleEntry implements Module{

    private Route route;

    @Override
    public void action(Route route) {

        this.route = route;
        Integer types = getTypes();

        new ModuleView(types);
    }

    @Override
    public String getName() {
        return "Steep slopes";
    }

    private Integer getTypes() {
        int count = 0;
        ArrayList<Point> track = route.getPath();
        int end = track.size() - 1;
        if (end < 1)
            return count;

        Point a, b;
        double angle, dist, height;
        angle = 0.;
        boolean lastCounted = false;
        for (int i = 0; i < end; i++)
        {
            double newAngle;
            a = track.get(i);
            b = track.get(i + 1);
            dist = a.distance(b);
            if (b.getAltitude() == null || a.getAltitude() == null)
            {
                continue;
            }
            if (b.getAltitude() == 0 && a.getAltitude() == 0)
            {
                angle = 0;
                continue;
            }

            height = Math.abs(b.getAltitude() - a.getAltitude());
            newAngle = Math.toDegrees(Math.asin(height/dist));

            if (Math.abs(angle) >= 8. && !lastCounted)
            {
                count++;
                lastCounted = true;
            }else if (Math.abs(angle) < 8.0) {
                lastCounted = false;
            }
            angle = newAngle;
        }

        return count;

    }
}
