import com.inwady.editor.Module;
import com.inwady.editor.Point;
import com.inwady.editor.Route;

import java.util.ArrayList;

public class SimpleModuleEntry implements Module {

    Route route;

    @Override
    public void action(Route route) {

        this.route = route;
        Integer[] types = getTypes();

        new ModuleView(types);
    }

    @Override
    public String getName() {
        return "Turns types";
    }

    private Integer[] getTypes() {

        ArrayList<Point> track = route.getPath();

        Integer types[] = {0, 0, 0, 0, 0, 0, 0, 0};

        int end = route.getPath().size() - 2;
        if (end < 1)
            return types;


        Point a, b, c;
        int type = 8;
        double angle = 180;
        for (int i = 0; i < end; i += 2)
        {
            double newAngle;
            a = track.get(i);
            b = track.get(i + 1);
            c = track.get(i + 2);

            newAngle = Math.toDegrees(anglePoints(a,b,c));
            System.out.println(newAngle);
            int newType = whichType(Math.abs(newAngle));
            if (newType != 8)
            {
                if ((newType != type) || (angle*newAngle < 0.))
                {
                    types[newType] += 1;
                }
            }
            type = newType;
            angle = newAngle;
        }

        for (int i = 0; i < types.length; i ++)
        {
            if (types[i] != 0)
                break;
            if (i == types.length - 1)
                return types;
        }

        return types;
    }

    private int whichType(double angle)
    {
        if (angle < 6.)
            return 0;
        else if (angle < 11.5)
            return 1;
        else if (angle < 34.)
            return 2;
        else if (angle < 56.5)
            return 3;
        else if (angle < 79.)
            return 4;
        else if (angle < 90.)
            return 5;
        else if (angle < 135.5)
            return 6;
        else if (angle < 160.)
            return 7;

        return 8;
    }

    private double anglePoints(Point a, Point b, Point c) {
        if ((b.distance(a) == 0) || (b.distance(c) == 0))
        {
            return 180;
        }
        Point v1 = diffPoints(a, b);
        Point v2 = diffPoints(c, b);

        double tmp = pointDot(v1,v2);
        double tmp2 = originDiff(v1);
        double tmp3 = originDiff(v2);

        return pointDot(v1,v2) / (originDiff(v1) * originDiff(v2));

    }

    private Point diffPoints(Point a, Point b) {
        return new Point(b.getLatitude() - a.getLatitude() ,
                b.getLongitude() - a.getLongitude());
    }

    private double originDiff(Point a) {
        return Math.sqrt(Math.pow(a.getLatitude(),2) + Math.pow(a.getLongitude(), 2) );
    }

    private double pointDot(Point a, Point b) {
        return a.getLongitude()*b.getLongitude() + a.getLatitude()*b.getLatitude();
    }
}
