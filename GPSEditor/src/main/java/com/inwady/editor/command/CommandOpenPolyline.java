package com.inwady.editor.command;

import com.inwady.editor.model.Model;
import com.inwady.editor.Point;
import com.inwady.editor.Route;
import com.inwady.editor.model.StateRoute;
import com.inwady.editor.utils.PolylineEncoder;
import com.inwady.editor.utils.PolylineEncoderException;
import com.inwady.editor.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class CommandOpenPolyline extends Command {
    private final File opened;
    public CommandOpenPolyline(Model model, File opened) {
        super(model);
        this.opened = opened;
    }

    @Override
    public boolean execute(){
        if (opened == null) {
            return false;
        }
        backup = model.getState();

        String polyline;
        try {
            polyline = Utils.readToLine(opened);
        } catch (IOException e) {
            return false;
        }

        ArrayList<Point> path;
        try {
            path = new ArrayList<>(PolylineEncoder.decode(polyline));
        } catch (PolylineEncoderException e) {
            return false;
        }

        Route route = new Route(opened.getName(), path, new Date());

        model.setState(StateRoute.NotModifiedRoute(route));

        return model.saveRoute();
    }

    @Override
    public void undo() {
        model.removeRoute();
        model.setState(backup);
    }
}
