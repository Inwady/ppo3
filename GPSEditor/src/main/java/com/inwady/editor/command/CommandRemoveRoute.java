package com.inwady.editor.command;

import com.inwady.editor.model.Model;

public class CommandRemoveRoute extends Command {

    public CommandRemoveRoute(Model model) {
        super(model);
    }

    @Override
    public boolean execute() {
        backup = model.getState();

        return !model.isNone() && model.removeRoute();

    }

    @Override
    public void undo() {
        model.setState(backup);
        model.saveRoute();
    }
}
