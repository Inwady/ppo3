package com.inwady.editor.command;

import com.inwady.editor.model.Model;
import com.inwady.editor.Point;
import com.inwady.editor.model.StateRoute;
import com.inwady.editor.utils.Utils;

public class CommandAddPoint extends Command {
    private final int index;

    public CommandAddPoint(Model model, int index) {
        super(model);
        this.index = index;
    }

    @Override
    public boolean execute() {
        if (model.isNone()) {
            return false;
        }
        backup = (StateRoute)Utils.deepClone(model.getState());
        model.getRoute().insertAfter(index, new Point(0.0,0.0));
        return true;
    }

    @Override
    public void undo() {
        model.setState(backup);
    }
}
