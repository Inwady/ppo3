package com.inwady.editor.command;

import com.inwady.editor.model.Model;
import com.inwady.editor.Point;
import com.inwady.editor.model.StateRoute;
import com.inwady.editor.utils.Utils;

public class CommandEditPoint extends Command {
    private final int index;
    private final Point point;

    public CommandEditPoint(Model model, int index, Point point) {
        super(model);
        this.index = index;
        this.point = point;
    }

    @Override
    public boolean execute() {
        if (model.isNone()) {
            return false;
        }
        backup = (StateRoute) Utils.deepClone(model.getState());
        model.getRoute().edit(index, point);

        System.out.println("edited");
        return true;
    }

    @Override
    public void undo() {
        model.setState(backup);
    }
}
