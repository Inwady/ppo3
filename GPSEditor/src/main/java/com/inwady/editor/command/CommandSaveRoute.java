package com.inwady.editor.command;

import com.inwady.editor.model.Model;
import com.inwady.editor.Route;
import com.inwady.editor.model.StateRoute;

public class CommandSaveRoute extends Command {
    private Route dbBackup;

    public CommandSaveRoute(Model model) {
        super(model);
    }

    @Override
    public boolean execute() {
        StateRoute state = model.getState();

        if(model.isNone()) {
            return false;
        }

        backup = model.getState();
        dbBackup = model.loadRouteByID(model.getRoute().getId());

        boolean result = model.updateRoute();

        if (result) {
            state.setNotModified();
        }

        return result;
    }

    @Override
    public void undo() {
        model.setState(StateRoute.ModifiedRoute(dbBackup));
        model.updateRoute();
        model.setState(backup);
    }
}
