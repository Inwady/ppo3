package com.inwady.editor.command;

public class CannotRedoException extends Exception {
    public CannotRedoException() {
        super("Redo error");
    }
}

