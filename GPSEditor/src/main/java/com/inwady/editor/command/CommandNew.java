package com.inwady.editor.command;

import com.inwady.editor.Constants;
import com.inwady.editor.model.Model;
import com.inwady.editor.Route;
import com.inwady.editor.model.StateRoute;

public class CommandNew extends Command {


    public CommandNew(Model model) {
        super(model);
    }

    @Override
    public boolean execute() {
        backup = model.getState();

        Route route = new Route();

        int index = model.getUntitledNextIndex();
        if (index != 0) {
            route.setName(String.format("%s(%d)", Constants.UNTITLED, index));
        }

        model.setState(StateRoute.NotModifiedRoute(route));

        return model.saveRoute();
    }

    @Override
    public void undo() {
        model.removeRoute();
        model.setState(backup);
    }

}
