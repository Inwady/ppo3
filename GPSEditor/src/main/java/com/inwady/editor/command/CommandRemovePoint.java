package com.inwady.editor.command;

import com.inwady.editor.model.Model;
import com.inwady.editor.model.StateRoute;
import com.inwady.editor.utils.Utils;

public class CommandRemovePoint extends Command {
    private final int index;

    public CommandRemovePoint(Model model, int index) {
        super(model);
        this.index = index;
    }

    @Override
    public boolean execute() {
        if (model.isNone()) {
            return false;
        }
        backup = (StateRoute) Utils.deepClone(model.getState());
        model.getRoute().remove(index);
        return true;
    }

    @Override
    public void undo() {
        model.setState(backup);
    }
}
