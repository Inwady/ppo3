package com.inwady.editor.command;

import com.inwady.editor.model.Model;
import com.inwady.editor.Route;
import com.inwady.editor.model.StateRoute;

public class CommandLoadRoute extends Command {
    private final int id;
    public CommandLoadRoute(Model model, int id) {
        super(model);
        this.id = id;
    }

    @Override
    public boolean execute() {
        Route route = model.loadRouteByID(id);

        backup = model.getState();

        if (route == null){
            return false;
        }

        model.setState(StateRoute.NotModifiedRoute(route));

        return true;
    }

    @Override
    public void undo() {
        model.setState(backup);
    }
}
