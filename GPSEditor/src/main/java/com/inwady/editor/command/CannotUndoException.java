package com.inwady.editor.command;

public class CannotUndoException extends Exception {
    public CannotUndoException() {
        super("Undo error");
    }
}
