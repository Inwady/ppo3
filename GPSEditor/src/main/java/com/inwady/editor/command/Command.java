package com.inwady.editor.command;

import com.inwady.editor.model.Model;
import com.inwady.editor.model.StateRoute;

public abstract class Command {
    final Model model;
    StateRoute backup;

    Command(Model model) {
        this.model = model;
    }

    public abstract boolean execute();

    public abstract void undo();
}
