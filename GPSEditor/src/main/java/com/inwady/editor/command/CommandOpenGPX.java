package com.inwady.editor.command;

import com.inwady.editor.model.Model;
import com.inwady.editor.Route;
import com.inwady.editor.model.StateRoute;
import com.inwady.editor.utils.GPXParseException;
import com.inwady.editor.utils.GPXReader;

import java.io.File;

public class CommandOpenGPX extends Command {
    private final File opened;

    public CommandOpenGPX(Model model, File opened) {
        super(model);
        this.opened = opened;
    }

    @Override
    public boolean execute() {
        if (opened == null) {
            return false;
        }

        backup = model.getState();

        Route route;
        try {
            route = GPXReader.parse(opened);
        } catch (GPXParseException e) {
            return false;
        }

        model.setState(StateRoute.NotModifiedRoute(route));

        return model.saveRoute();
    }

    @Override
    public void undo() {
        model.removeRoute();
        model.setState(backup);
    }
}
