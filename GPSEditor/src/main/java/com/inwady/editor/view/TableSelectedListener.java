package com.inwady.editor.view;

public interface TableSelectedListener {
    boolean select(int id);
}
