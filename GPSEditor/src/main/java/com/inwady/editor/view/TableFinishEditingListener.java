package com.inwady.editor.view;

import com.inwady.editor.Point;

public interface TableFinishEditingListener {
    void edited(int index, Point point);
}
