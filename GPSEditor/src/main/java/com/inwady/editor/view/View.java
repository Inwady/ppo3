package com.inwady.editor.view;

import com.inwady.editor.controller.Controller;

import javax.swing.*;

public abstract class View extends JPanel {
    private Controller controller;

    public Controller getController() {
        return controller;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }


    public void warningMessage(String text) {}

    public void errorMessage(String text) {}
}
