package com.inwady.editor.view;

import com.inwady.editor.Module;
import com.inwady.editor.Route;
import com.inwady.editor.controller.EditorController;
import com.inwady.editor.controller.RouteDataTableController;
import com.inwady.editor.controller.RoutesTableController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;

public class EditorView extends View {
    private JPanel mainPanel;

    public JButton openGPXButton;
    public JButton openPolylineButton;
    public JButton undoButton;
    public JButton redoButton;
    public JButton newButton;
    public JButton saveButton;
    public JButton addPointAfterSelectedButton;
    public JButton removeSelectedPointButton;
    public JButton removeSelectedRouteButton;

    public JButton loadModuleButton;

    private RoutesTable routesTable;
    private RouteDataTable routeDataTable;

    private JPanel modulesPanel;


    public EditorView() {
        setupGUI();

        setController(new EditorController(this,
                (RouteDataTableController) routeDataTable.getController(),
                (RoutesTableController) routesTable.getController()));
    }

    private void setupGUI() {
        JFrame frame = new JFrame("GPS EditorView");

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setContentPane(mainPanel);
        frame.setResizable(false);
        frame.pack();
        frame.setSize(new Dimension(1400, 600));
        frame.setVisible(true);
    }

    @Override
    public void warningMessage(String text) {
        JOptionPane.showMessageDialog(null, text);
    }

    @Override
    public void errorMessage(String text) {
        JOptionPane.showMessageDialog(null, text);
    }

    public void updateModules(List<Module> modules, ModuleListener l) {
        modulesPanel.removeAll();
        modulesPanel.revalidate();
        modulesPanel.setLayout(new BoxLayout(modulesPanel, BoxLayout.Y_AXIS));

        for (Module module : modules) {
            JButton button = new JButton();
            String name = module.getName();
            button.setText(name);

            final Module tempModel = module;
            button.addActionListener((e) -> {
                l.message(tempModel);
            });
            modulesPanel.add(button);
        }

        modulesPanel.revalidate();
    }

}
