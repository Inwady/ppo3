package com.inwady.editor.view;

import com.inwady.editor.Module;

public interface ModuleListener {
    void message(Module name);
}
