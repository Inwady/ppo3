package com.inwady.editor.view;

public interface TableSelectionListener {
    boolean canSelect();
}
