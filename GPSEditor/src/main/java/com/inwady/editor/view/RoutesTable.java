package com.inwady.editor.view;

import com.inwady.editor.Route;
import com.inwady.editor.controller.RoutesTableController;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DateFormat;
import java.util.*;
import java.util.List;

public class RoutesTable extends View {
    public JTable table;

    private JPanel panel;

    private class DateCellRenderer extends DefaultTableCellRenderer {
        DateFormat formatter;

        DateCellRenderer() {
            super();
        }

        public void setValue(Object value) {
            if (formatter == null) {
                String systemLocale = System.getProperty("user.language");
                Locale locale = new Locale(systemLocale);
                formatter = DateFormat.getDateInstance(DateFormat.MEDIUM, locale);
            }

            setText((value == null) ? "" : formatter.format(value));
        }
    }


    public RoutesTable() {
        super();

        setLayout(new BorderLayout());
        setController(new RoutesTableController(this));

        table.getColumnModel().getColumn(2).setCellRenderer(new DateCellRenderer());
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        MouseListener[] l = table.getMouseListeners();
        table.removeMouseListener(l[1]);

        add(panel);
    }
}
