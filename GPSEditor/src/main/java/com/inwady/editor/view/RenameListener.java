package com.inwady.editor.view;

public interface RenameListener {
    void rename(String name);
}
