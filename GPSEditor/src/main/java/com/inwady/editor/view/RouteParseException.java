package com.inwady.editor.view;

public class RouteParseException extends Exception {
    public RouteParseException() {
        super("Route parsing error");
    }
}
