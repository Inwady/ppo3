package com.inwady.editor.view;

import com.inwady.editor.Point;
import com.inwady.editor.Route;
import com.inwady.editor.controller.Controller;
import com.inwady.editor.controller.RouteDataTableController;
import com.inwady.editor.utils.PolylineEncoder;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Date;

public class RouteDataTable extends View {
    public JTable table;

    private JPanel panel;

    public JButton copyToClipboardButton;
    public JTextField routeNameField;

    // chart panel
    public JPanel chartPanel;

    public Date creationDate;

    public RouteDataTable() {
        setLayout(new BorderLayout());
        setController(new RouteDataTableController(this));
    }

    public void setHeights(ArrayList<Point> path) {
        chartPanel.removeAll();
        chartPanel.revalidate();
        chartPanel.repaint();
        JFreeChart xylineChart = ChartFactory.createXYLineChart(
                "Map heights",
                "Distance",
                "Height",
                createDataset(path),
                PlotOrientation.VERTICAL,
                true, true, false);

        ChartPanel chartPanelView = new ChartPanel(xylineChart);
        chartPanelView.setPreferredSize(new Dimension(500, 500));
        chartPanel.add(chartPanelView);
    }

    private XYDataset createDataset(ArrayList<Point> path) {
        if (path.size() == 0) {
            return new XYSeriesCollection();
        }

        final XYSeries heights = new XYSeries("Heights");
        double dist = 0;

        heights.add(dist, path.get(0).getAltitude());
        for (int i = 1; i < path.size(); i++) {
            dist += path.get(i - 1).distance(path.get(i));
            heights.add(dist, path.get(i).getAltitude());
        }

        final XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(heights);
        return dataset;
    }
}
