package com.inwady.editor;

public class Constants {
    enum Enviroment {DEVELOPMENT, TEST, PRODUCTION};

    private static final Enviroment mode = Enviroment.TEST;
    public static String configPath;
    public static String modulePath;
    public static final int CANNOT_CREATE_CONFIG_FOLDER = 1;
    public static final int CANNOT_CREATE_DATABASE = 2;
    public static final String UNTITLED = "untitled";

    static {
        System.out.println(System.getProperty("user.home"));

        switch (mode) {
            case TEST:
                configPath = String.format("./GPSEditor/Test/");
                break;
            case DEVELOPMENT:
                configPath = String.format("./GPSEditor/Dev/");
                break;
            case PRODUCTION:
                configPath = String.format("./.config/GPSEditor/");
                break;
        }

        modulePath = configPath + "modules/";
    }

}
