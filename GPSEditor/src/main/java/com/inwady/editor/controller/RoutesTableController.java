package com.inwady.editor.controller;

import com.inwady.editor.Module;
import com.inwady.editor.Route;
import com.inwady.editor.model.event.EventSystem;
import com.inwady.editor.view.RouteDataTable;
import com.inwady.editor.view.RoutesTable;
import com.inwady.editor.view.TableSelectedListener;
import com.inwady.editor.view.TableSelectionListener;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

public class RoutesTableController extends Controller {
    private ArrayList<Route> mRoutes;

    private RoutesTable mRoutesTable;

    private int index;
    private final ArrayList<TableSelectionListener> tableSelectionListeners;
    private final ArrayList<TableSelectedListener> tableSelectedListeners;

    public RoutesTableController(RoutesTable routesTable) {
        super(routesTable);

        index = -1;
        mRoutes = new ArrayList<>();

        tableSelectionListeners = new ArrayList<>();
        tableSelectedListeners = new ArrayList<>();

        mRoutesTable = routesTable;

        mRoutesTable.table.setModel(new DataModel());
        mRoutesTable.table.addMouseListener(new MouseAdapter() {
            private void setSelection(int index) {
                JTable table = mRoutesTable.table;

                if (index < 0 || index >= table.getRowCount()) {
                    table.clearSelection();
                } else {
                    table.setRowSelectionInterval(index, index);
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                JTable table = mRoutesTable.table;

                int row = table.rowAtPoint(e.getPoint());
                if (row < 0 || row >= table.getRowCount() || row == index) {
                    return;
                }

                for (TableSelectionListener listener : tableSelectionListeners) {
                    if (!listener.canSelect()) {
                        setSelection(index);
                        return;
                    }
                }

                for (TableSelectedListener listener : tableSelectedListeners) {
                    if (!listener.select(getIDAt(row))) {
                        setSelection(index);
                        return;
                    }
                }

                index = row;
                setSelection(index);
            }
        });
    }

    private class DataModel extends DefaultTableModel {
        final String[] columns = {"Name", "Length", "Creation date"};

        public String getColumnName(int col) {
            return columns[col];
        }

        public int getColumnCount() {
            return columns.length;
        }
    }

    private DataModel getModel() {
        return (DataModel) mRoutesTable.table.getModel();
    }

    @Override
    void setEventBus() {
        EventSystem.getInstance().subscribe(new EventSystem.EventBus() {
            @Override
            public void update(List<Route> routes) {
                mRoutes = new ArrayList<>(routes);

                System.out.println(routes);

                clear();

                for (Route route : mRoutes) {
                    addRoute(route);
                }

                cancelSelection();
            }

            @Override
            public void update(List<Route> routes, Route selected) {
                mRoutes = new ArrayList<>(routes);

                System.out.println(routes);

                clear();

                int index = 0;
                for (Route route : mRoutes) {
                    addRoute(route);
                    if (route.getId() == selected.getId()) {
                        setSelection(index);
                    }
                    index++;
                }
            }

            @Override
            public void updateModule(List<Module> visitors) {}
        });
    }

    private void addRoute(Route route) {
        DataModel model = (DataModel) mRoutesTable.table.getModel();
        model.addRow(new Object[]{route.getName(), route.getLength(), route.getDate()});
    }

    private void clear() {
        DataModel model = getModel();
        model.setRowCount(0);
    }

    public void addTableSelectionListener(TableSelectionListener listener) {
        tableSelectionListeners.add(listener);
    }

    public void addTableSelectedListener(TableSelectedListener listener) {
        tableSelectedListeners.add(listener);
    }

    public void setSelection(int i) {
        index = i;
        mRoutesTable.table.setRowSelectionInterval(i, i);
    }

    public void cancelSelection() {
        index = -1;
        mRoutesTable.table.clearSelection();
    }

    public int getSelectedIndex() {
        return mRoutesTable.table.getSelectedRow();
    }

    private int getIDAt(int i) {
        return mRoutes.get(i).getId();
    }
}
