package com.inwady.editor.controller;

import com.inwady.editor.Module;
import com.inwady.editor.Route;
import com.inwady.editor.command.CommandInvoker;
import com.inwady.editor.command.CommandLoadRoute;
import com.inwady.editor.model.event.EventSystem;
import com.inwady.editor.view.EditorView;
import com.inwady.editor.view.RoutesTable;
import com.inwady.editor.view.TableSelectedListener;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.io.File;
import java.util.List;

public class EditorController extends Controller {
    private EditorView editorView;

    private RouteDataTableController mRouteDataTableController;
    private RoutesTableController mRoutesTableController;

    private Route route;

    protected List<Module> modules;

    public EditorController(EditorView mEditorView,
                            RouteDataTableController routeDataController,
                            RoutesTableController routesTableController) {
        super(mEditorView);

        mRouteDataTableController = routeDataController;
        mRoutesTableController = routesTableController;

        setControllers();

        editorView = mEditorView;
        addNewButton(editorView.newButton, () -> {
            if (!saveIfNeeded()) {
                return;
            }

            model.newRoute();
        });

        addNewButton(editorView.openGPXButton, () -> {
            if (!saveIfNeeded()) {
                return;
            }

            File opened = openDialog(new FileFilter() {
                public String getDescription() {
                    return "GPX file (*.gpx)";
                }

                public boolean accept(File f) {
                    return f.isDirectory() || f.getName().toLowerCase().endsWith(".gpx");
                }
            }, "Open GPX");

            if (opened != null) {
                model.openGPX(opened);
            }
        });

        addNewButton(editorView.openPolylineButton, () -> {
            if (!saveIfNeeded()) {
                return;
            }

            File opened = openDialog(null, "Open Polyline");

            if (opened != null) {
                model.openPolyline(opened);
            }
        });


        addNewButton(editorView.removeSelectedRouteButton, () -> {
            model.removeSelectedRoute();
        });

        addNewButton(editorView.saveButton, () -> {
            model.saveRoute();
        });

        // undo + redo
        addNewButton(editorView.undoButton, () -> {
            model.undo();
        });

        addNewButton(editorView.redoButton, () -> {
            model.redo();
        });

        // points
        addNewButton(editorView.addPointAfterSelectedButton, () -> {
            int index = routesTableController.getSelectedIndex();
            model.addPointAfterSelected(index);
        });

        addNewButton(editorView.removeSelectedPointButton, () -> {
            int index = routesTableController.getSelectedIndex();
            model.removeSelectedPoint(index);
        });

        addNewButton(editorView.loadModuleButton, () -> {
            File opened = openDialog(new FileFilter() {
                public String getDescription() {
                    return "Jar file (*.jar)";
                }

                public boolean accept(File f) {
                    return f.isDirectory() || f.getName().toLowerCase().endsWith(".jar");
                }
            }, "Open Jar");

            if (opened != null) {
                model.newModule(opened);
            }
        });
    }

    private void setControllers() {
        mRoutesTableController.addTableSelectionListener(() -> {
            try {
                saveIfNeeded();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return true;
        });

        mRoutesTableController.addTableSelectedListener((id) -> model.select(id));

        mRouteDataTableController.setFinishEditingListener((index, point) -> {
            try {
                model.edited(index, point);
            } catch (Exception e) {
                editorView.errorMessage(e.getMessage());
            }
        });

        mRouteDataTableController.setRenameListener((newName) -> {
            try {
                model.rename(newName);
            } catch (Exception e) {
                editorView.errorMessage(e.getMessage());
            }
        });
    }

    @Override
    void setEventBus() {
        EventSystem.getInstance().subscribe(new EventSystem.EventBus() {
            @Override
            public void update(List<Route> routes) {
                route = null;
            }

            @Override
            public void update(List<Route> routes, Route _route) {
                route = _route;
            }

            @Override
            public void updateModule(List<Module> visitors) {
                modules = visitors;
                editorView.updateModules(modules, module -> {
                    if (route != null)
                        module.action(route);
                    else
                       editorView.warningMessage("Please, choose route!");
                });
            }
        });
    }

    private boolean saveIfNeeded() throws Exception {
        if (model.needSave()) {
            int code = checkSaveDialog();
            switch (code) {
                case JOptionPane.CANCEL_OPTION:
                    return false;
                case JOptionPane.NO_OPTION:
                    break;
                case JOptionPane.YES_OPTION:
                    model.saveRoute();
                    break;
            }
        }
        return true;
    }
}
