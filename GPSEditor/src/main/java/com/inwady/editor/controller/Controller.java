package com.inwady.editor.controller;

import com.inwady.editor.model.BasicModel;
import com.inwady.editor.utils.CallbackManager;
import com.inwady.editor.view.View;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public abstract class Controller implements ActionListener {
    private CallbackManager<JButton> callbackManager;
    private View view;

    protected BasicModel model = BasicModel.getInstance();


    public Controller(View mView) {
        view = mView;
        callbackManager = new CallbackManager<>();

        setEventBus();
    }

    abstract void setEventBus();

    protected void addNewButton(JButton button, CallbackManager.Handler<JButton> handler) {
        button.addActionListener(this);
        callbackManager.addHandler(button, handler);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        try {
            callbackManager.call((JButton) event.getSource());
        } catch (Exception e) {
            view.errorMessage(e.getMessage());
        }
    }

    protected int checkSaveDialog() {
        return JOptionPane.showConfirmDialog(null,
                "Save?",
                "Warning",
                JOptionPane.YES_NO_CANCEL_OPTION);
    }

    protected File openDialog(FileFilter filter, String header) {
        JFileChooser openDialog = new JFileChooser();
        if (filter != null) {
            openDialog.addChoosableFileFilter(filter);
        }
        int ret = openDialog.showDialog(null, header);
        if (ret == JFileChooser.CANCEL_OPTION) {
            return null;
        }
        return openDialog.getSelectedFile();
    }
}
