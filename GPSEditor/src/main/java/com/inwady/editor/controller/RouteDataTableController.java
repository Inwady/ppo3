package com.inwady.editor.controller;

import com.inwady.editor.Module;
import com.inwady.editor.Point;
import com.inwady.editor.Route;
import com.inwady.editor.model.event.EventSystem;
import com.inwady.editor.utils.PolylineEncoder;
import com.inwady.editor.view.RenameListener;
import com.inwady.editor.view.RouteDataTable;
import com.inwady.editor.view.RouteParseException;
import com.inwady.editor.view.TableFinishEditingListener;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;

public class RouteDataTableController extends Controller implements FocusListener, TableModelListener {
    private RenameListener renameListener;
    private TableFinishEditingListener tableFinishEditingListener;

    RouteDataTable mRouteDataTable;

    private String polyline;

    public RouteDataTableController(RouteDataTable routeDataTable) {
        super(routeDataTable);

        mRouteDataTable = routeDataTable;

        renameListener = null;
        tableFinishEditingListener = null;

        mRouteDataTable.routeNameField.addFocusListener(this);

        mRouteDataTable.table.setModel(new DataModel());
        mRouteDataTable.table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        mRouteDataTable.table.getModel().addTableModelListener(this);

        addNewButton(mRouteDataTable.copyToClipboardButton, () -> {
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Clipboard clipboard = toolkit.getSystemClipboard();
            StringSelection strSel = new StringSelection(polyline);
            clipboard.setContents(strSel, null);
        });
    }

    private class DataModel extends DefaultTableModel {
        final String[] columns = {"Latitude", "Longitude", "Altitude"};

        public String getColumnName(int col) {
            return columns[col];
        }

        public int getColumnCount() {
            return columns.length;
        }


        public Class getColumnClass(int c) {
            return Double.class;
        }
    }

    private DataModel getModel() {
        return (DataModel) mRouteDataTable.table.getModel();
    }

    @Override
    void setEventBus() {
        EventSystem.getInstance().subscribe(new EventSystem.EventBus() {
            @Override
            public void update(List<Route> routes) {
                clearData();
            }

            @Override
            public void update(List<Route> routes, Route route) {
                if (route == null) {
                    getModel().setRowCount(0);
                    return;
                }

                System.out.println(routes);

                mRouteDataTable.creationDate = route.getDate();
                mRouteDataTable.routeNameField.setText(route.getName());

                polyline = PolylineEncoder.encode(route.getPath());

                getModel().setRowCount(0);

                ArrayList<Point> path = route.getPath();
                for (Point point : path) {
                    addRow(point.getLatitude(), point.getLongitude(), point.getAltitude());
                }

                mRouteDataTable.setHeights(path);
            }

            @Override
            public void updateModule(List<Module> visitors) {}
        });
    }

    @Override
    public void focusGained(FocusEvent e) {}

    @Override
    public void focusLost(FocusEvent e) {
        if (!e.isTemporary()) {
            System.out.println("test");
            if (renameListener != null) {
                renameListener.rename(mRouteDataTable.routeNameField.getText());
            }
        }
    }

    public Route getRoute() throws RouteParseException {
        String name = mRouteDataTable.routeNameField.getText();
        ArrayList<Point> path = new ArrayList<>();
        DataModel model = getModel();
        int nRow = model.getRowCount();

        try {
            Double latitude, longitude, atitude;
            for (int i = 0; i < nRow; i++) {
                latitude = Double.parseDouble(model.getValueAt(i, 0).toString());
                longitude = Double.parseDouble(model.getValueAt(i, 1).toString());
                atitude = Double.parseDouble(model.getValueAt(i, 2).toString());
                Point point = new Point(latitude, longitude, atitude);
                path.add(point);
            }
        } catch (NumberFormatException | NullPointerException ignored) {
            throw new RouteParseException();
        }

        return new Route(name, path, mRouteDataTable.creationDate);
    }

    public void setFinishEditingListener(TableFinishEditingListener listener) {
        tableFinishEditingListener = listener;
    }

    public void setRenameListener(RenameListener listener) {
        renameListener = listener;
    }

    public void clearData() {
        mRouteDataTable.routeNameField.setText("");
        polyline = "";
        getModel().setRowCount(0);
    }

    private void addRow(double lat, double lng, Double att) {
        DataModel model = getModel();
        int index = model.getRowCount();
        model.insertRow(index, new Object[]{lat, lng, att});
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        if (e.getType() == TableModelEvent.UPDATE) {
            int i = mRouteDataTable.table.getSelectedRow();
            DataModel dataModel = getModel();
            Point point;
            Double latitude, longitude, altitude;
            try {
                latitude = Double.parseDouble(dataModel.getValueAt(i, 0).toString());
                longitude = Double.parseDouble(dataModel.getValueAt(i, 1).toString());
                Object value = dataModel.getValueAt(i, 2);
                if (value == null) {
                    altitude = null;
                } else {
                    altitude = Double.parseDouble(value.toString());
                }
                point = new Point(latitude, longitude, altitude);
                if (tableFinishEditingListener != null) {
                    tableFinishEditingListener.edited(i, point);
                }
            } catch (NumberFormatException | NullPointerException ignored) {
            }
        }
    }
}
