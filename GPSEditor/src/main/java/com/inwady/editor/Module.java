package com.inwady.editor;

public interface Module {
    void action(Route route);
    String getName();
}
