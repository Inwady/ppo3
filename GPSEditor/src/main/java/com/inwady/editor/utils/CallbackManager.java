package com.inwady.editor.utils;

import java.util.HashMap;
import java.util.Map;

public class CallbackManager<T> {
    public interface Handler<T> {
        void message() throws Exception;
    }

    private Map<T, Handler<T>> callbackMap;

    public CallbackManager() {
        callbackMap = new HashMap<>();
    }

    public void addHandler(T key, Handler<T> handler) {
        callbackMap.put(key, handler);
    }

    public void call(T key) throws Exception {
        // with runtime assert
        callbackMap.get(key).message();
    }
}
