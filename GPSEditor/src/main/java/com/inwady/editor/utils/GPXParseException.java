package com.inwady.editor.utils;

public class GPXParseException extends Exception  {
    public GPXParseException() {}

    public GPXParseException(String message) {
        super(message);
    }
}
