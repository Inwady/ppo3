package com.inwady.editor.utils;

public class PolylineEncoderException extends Exception  {
    public PolylineEncoderException() { }

    public PolylineEncoderException(String message) {
        super(message);
    }
}
