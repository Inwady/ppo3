package com.inwady.editor.utils.serialize;

public class SerializeException extends Exception  {
    public SerializeException() {
    }

    public SerializeException(String message) {
        super(message);
    }
}
