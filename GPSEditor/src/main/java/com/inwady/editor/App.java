package com.inwady.editor;

import com.inwady.editor.model.DatabaseModel;
import com.inwady.editor.model.event.EventSystem;
import com.inwady.editor.view.EditorView;
import com.inwady.editor.view.View;
import com.inwady.editor.model.BasicModel;
import com.inwady.editor.utils.Utils;
import io.methvin.watcher.DirectoryWatcher;

import javax.swing.*;
import java.io.IOException;
import java.nio.file.Paths;

class App {

    public static void main(String[] args) {
        App app = new App();
        app.startApp();
    }

    private void startApp() {
        SwingUtilities.invokeLater(() -> {
            try {
                Class.forName("org.sqlite.JDBC");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                System.exit(1);
            }

            // only for mac os
            System.setProperty("apple.laf.useScreenMenuBar", "true");
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Test");
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (ClassNotFoundException | UnsupportedLookAndFeelException | IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }

            Utils.createConfigDir();

            new EditorView();

            // update components
            BasicModel.getInstance().updateModulesDirectory();
            BasicModel.getInstance().updateState();

            // start visor
            try {
                DirectoryWatcher.create(Paths.get(Constants.modulePath), event -> {
                    switch (event.eventType()){
                        case CREATE:
                        case DELETE:
                            BasicModel.getInstance().updateModulesDirectory();
                            break;
                        default:
                            break;
                    }
                }).watchAsync();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
