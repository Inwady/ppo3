package com.inwady.editor.model;

import com.inwady.editor.command.*;
import com.inwady.editor.Constants;
import com.inwady.editor.Module;
import com.inwady.editor.Point;
import com.inwady.editor.model.event.EventSystem;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.*;
import java.util.ArrayList;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class BasicModel {
    private final Model model;

    private ArrayList<Module> modules;

    private EventSystem eventSystem = EventSystem.getInstance();

    private static BasicModel instance = new BasicModel(new DatabaseModel());
    public static BasicModel getInstance() {
        return instance;
    }

    private BasicModel(Model model) {
        this.model = model;
        this.modules = new ArrayList<>();
    }

    public void updateModulesDirectory() {
        modules.clear();

        File folder = new File(Constants.modulePath);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < (listOfFiles != null ? listOfFiles.length : 0); i++) {
            if (listOfFiles[i].isFile()) {
                String name = listOfFiles[i].getName();
                try {
                    if (name.substring(name.lastIndexOf(".") + 1).equals("jar")){
                        addModule(listOfFiles[i]);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return ;
                }
            }
        }
    }

    public void newRoute() throws Exception {
        CommandInvoker invoker = model.getCommandInvoker();
        if (!invoker.executeCommand(new CommandNew(model))) {
            throw new Exception("Cannot create new route");
        }

        updateState();
    }

    public void openGPX(File opened) throws Exception {
        CommandInvoker invoker = model.getCommandInvoker();
        if (!invoker.executeCommand(new CommandOpenGPX(model, opened))) {
            throw new Exception("Cannot open gpx");
        }

        updateState();
    }

    public void openPolyline(File opened) throws Exception {
        CommandInvoker invoker = model.getCommandInvoker();
        if (!invoker.executeCommand(new CommandOpenPolyline(model, opened))) {
            throw new Exception("Cannot open polyline");
        }
        updateState();
    }

    public void removeSelectedRoute() throws Exception {
        if(model.isNone()) {
            throw new Exception("No route to edit");
        }

        CommandInvoker invoker = model.getCommandInvoker();
        if (!invoker.executeCommand(new CommandRemoveRoute(model))) {
            throw new Exception("Cannot remove route");
        }

        updateState();
    }

    public void addPointAfterSelected(int index) throws Exception {
        if(model.isNone()) {
            throw new Exception("No route to edit");
        }

        CommandInvoker invoker = model.getCommandInvoker();
        if (!invoker.executeCommand(new CommandAddPoint(model, index))) {
            throw new Exception("Cannot add point");
        }

        updateState();
    }

    public void removeSelectedPoint(int index) throws Exception {
        if(model.isNone()) {
            throw new Exception("No route to edit");
        }

        CommandInvoker invoker = model.getCommandInvoker();
        if (!invoker.executeCommand(new CommandRemovePoint(model, index))) {
            throw new Exception("Cannot remove point");
        }

        updateState();
    }

    public void rename(String name) throws Exception {
        if(model.isNone()) {
            throw new Exception("No route to rename");
        }

        CommandInvoker invoker = model.getCommandInvoker();
        if (!invoker.executeCommand(new CommandRenameRoute(model, name))) {
            throw new Exception("Cannot rename route");
        }

        model.updateRoute();
        updateState();
    }

    public void saveRoute() throws Exception {
        if(model.isNone()) {
            throw new Exception("No route to save");
        }

        CommandInvoker invoker = model.getCommandInvoker();
        if (!invoker.executeCommand(new CommandSaveRoute(model))) {
            throw new Exception("Cannot save route");
        }

        updateState();
    }


    public void undo() throws Exception {
        CommandInvoker invoker = model.getCommandInvoker();
        try {
            invoker.undo();
            model.updateRoute();
            updateState();
        } catch (CannotUndoException e) {
            throw new Exception("Cannot undo");
        }
    }

    public void redo() throws Exception {
        CommandInvoker invoker = model.getCommandInvoker();
        try {
            invoker.redo();
            model.updateRoute();
            updateState();
        } catch (CannotRedoException e) {
            throw new Exception("Cannot redo");
        }
    }

    public boolean needSave() {
        StateRoute route = model.getState();
        return route.isModified();
    }

    public void edited(int index, Point point) throws Exception {
        CommandInvoker invoker = model.getCommandInvoker();
        if (!invoker.executeCommand(new CommandEditPoint(model, index, point))) {
            throw new Exception("Cannot rename route");
        }

        updateState();
    }

    public boolean select(int id) {
        CommandInvoker invoker = model.getCommandInvoker();
        if (!invoker.executeCommand(new CommandLoadRoute(model, id))) {
            System.out.println("Cannot rename route");

            updateState();
            return false;
        }

        updateState();
        return true;
    }

    public boolean canSelect() {
        return needSave();
    }

    public void newModule(File jar) {
        Path src = jar.toPath();
        Path dst = Paths.get(Constants.modulePath + jar.getName());
        try {
            Files.copy(src, dst, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateState() {
        if (!model.isNone()) {
            eventSystem.broadcast((e) -> { e.update(model.loadAllRoutes(), model.getRoute()); });
        } else {
            eventSystem.broadcast((e) -> { e.update(model.loadAllRoutes()); });
        }
    }

    private void addModule(File jar) throws Exception {
        boolean success = false;

        try
        {
            JarFile jarFile = new JarFile(jar);
            Enumeration<JarEntry> e = jarFile.entries();

            URL[] urls = { new URL("jar:file:" + jar + "!/") };
            URLClassLoader cl = URLClassLoader.newInstance(urls);

            System.out.println("!!!");
            System.out.println(urls);

            while (e.hasMoreElements()) {
                JarEntry je = e.nextElement();
                if(je.isDirectory() || !je.getName().endsWith(".class")){
                    continue;
                }

                System.out.println(je.getName());

                String className = je.getName();

                System.out.println(className);
                System.out.println(toHex(className));
                System.out.println(toHex("SimpleModule"));


                if (className.contains("Entry")) {
                    System.out.println("started 1");
                    Class c = cl.loadClass(className.replace(".class", ""));
                    System.out.println("started 2");

                    Constructor cs = c.getConstructor();

                    System.out.println("added");

                    Module instance = (Module) cs.newInstance();

                    System.out.println("added");
                    System.out.println(instance.getName());


                    modules.add(instance);

                    eventSystem.broadcast((event) -> {
                        event.updateModule(modules);
                    });

                    success = true;
                }
            }
        }
        catch (IOException | InstantiationException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        if (!success) {
            throw new Exception("cannot load file: " + jar.getName());
        }
    }

    public String toHex(String arg) {
        return String.format("%040x", new BigInteger(1, arg.getBytes(/*YOUR_CHARSET?*/)));
    }
}
