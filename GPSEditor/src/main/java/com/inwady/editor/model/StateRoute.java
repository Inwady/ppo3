package com.inwady.editor.model;

import com.inwady.editor.Route;

import java.io.Serializable;

public class StateRoute implements Serializable {
    private enum RouteState  implements Serializable {NONE, NEW, MODIFIED, NOT_MODIFIED}

    private Route route;
    private RouteState state;

    public StateRoute() {
        this.route = null;
        this.state = RouteState.NONE;
    }

    private StateRoute(Route route, RouteState state) {
        this.state = state;
        this.route = route;
    }

    public static StateRoute NoneRoute() {
        return new StateRoute(null, RouteState.NONE);
    }

    public static StateRoute ModifiedRoute(Route route) {
        return new StateRoute(route, RouteState.MODIFIED);
    }

    public static StateRoute NotModifiedRoute(Route route) {
        return new StateRoute(route, RouteState.NOT_MODIFIED);
    }

    public Route getRoute() {
        return route;
    }

    public boolean isNone(){
        return state == RouteState.NONE;
    }

    public boolean isModified() {
        return state == RouteState.MODIFIED;
    }

    public void setNotModified() {
        state = RouteState.NOT_MODIFIED;
    }

}
