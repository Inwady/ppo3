package com.inwady.editor.model.event;

import com.inwady.editor.Module;
import com.inwady.editor.Route;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class EventSystem {
     public interface EventBus {
        public abstract void update(List<Route> routes);

        public abstract void update(List<Route> routes, Route route);

        public abstract void updateModule(List<Module> visitors);
    }

    public interface EventBroadcast {
         void Send(EventBus eb);
    }

    private List<EventBus> subscribes;
    private EventSystem() {
         subscribes = new ArrayList<>();
    }

    public void subscribe(EventBus eventer) {
        subscribes.add(eventer);
    }

    public void broadcast(EventBroadcast eventBroadcast) {
        subscribes.forEach(eventBroadcast::Send);
    }

    private static EventSystem eventSystem = new EventSystem();
    public static EventSystem getInstance() {
        return eventSystem;
    }
}
