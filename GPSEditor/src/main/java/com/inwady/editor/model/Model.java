package com.inwady.editor.model;

import com.inwady.editor.command.CommandInvoker;
import com.inwady.editor.Route;

import java.util.List;

public interface Model {
    int getUntitledNextIndex();

    boolean saveRoute();

    boolean updateRoute();

    boolean removeRoute();

    Route loadRouteByID(int id);

    List<Route> loadAllRoutes();

    StateRoute getState();

    void setState(StateRoute state);

    boolean isNone();

    Route getRoute();

    CommandInvoker getCommandInvoker();
}
