import com.inwady.editor.Module;
import com.inwady.editor.Point;
import com.inwady.editor.Route;

import java.util.ArrayList;

public class SlopesModuleEntry implements Module {

    private Route route;

    @Override
    public void action(Route route) {

        this.route = route;
        Integer[] types = getTypes();

        new ModuleView(types);
    }

    @Override
    public String getName() {
        return "Slopes types";
    }

    private Integer[] getTypes() {
        Integer[] types = {0,0,0,0,0};
        ArrayList<Point> track = route.getPath();
        int end = track.size() - 1;
        if (end < 1)
            return types;

        Point a, b;
        double dist, height, angle = 0, currentLen = 0;
        int  type = -1;

        boolean lastCounted = false;
        for (int i = 0; i < end; i++)
        {
            double newAngle;
            a = track.get(i);
            b = track.get(i + 1);
            dist = a.distance(b);
            if (b.getAltitude() == null || a.getAltitude() == null)
            {
                continue;
            }

            height = Math.abs(b.getAltitude() - a.getAltitude());
            newAngle = Math.toDegrees(Math.asin(height/dist));

            int newType = whichType(Math.abs(angle));
            if (newType != -1 && !lastCounted) {
                lastCounted = true;
                currentLen += dist;
                type = newType;
            } else if (lastCounted) {
                if (newType != -1 && newAngle *angle > 0) {
                    currentLen += dist;
                    if (newType > type) {
                        type = newType;
                    }
                }else {
                    types[type] += acceptingSlope(type, currentLen);

                    lastCounted = false;
                    currentLen = 0;
                    type = -1;

                }
            }

            angle = newAngle;

        }

        return types;
    }


    int whichType(double angle)
    {
        if (angle >= 2 && angle < 4.)
            return 0;
        else if (angle >= 4. && angle < 8.)
            return 1;
        else if (angle >= 8. && angle < 15.)
            return 2;
        else if (angle >= 15. && angle < 35.)
            return 3;
        else if (angle >= 35.)
            return 4;

        return -1;
    }

    int acceptingSlope(int type, double slopeLen)
    {
        switch (type) {
            case 0:
                if (slopeLen >= 600)
                    return 1;
                break;
            case 1:
                if (slopeLen >= 450)
                    return 1;
                break;
            case 2:
                if (slopeLen >= 350)
                    return 1;
                break;
            case 3:
                if (slopeLen >= 300)
                    return 1;
                break;
            case 4:
                if (slopeLen >= 270)
                    return 1;
                break;
            default:
                return 0;
        }

        return 0;
    }
}
